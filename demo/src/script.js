import CookieFZ from '../../dist/cookiefz'

const params = {
  id: 'G-XXXXXX',
  alwaysShowBanner: true,
  title: 'Nous utilisons des cookies',
  text: 'Nous améliorons ainsi votre expérience de navigation sur notre site web. Les cookies nous permettent de vous fournir des <a href="#">fonctionnalités importantes</a> liées à la sécurité, à la gestion du web et/ou à l’accessibilité.'
}

const cookieBanner = new CookieFZ(params, function () {
  console.log('cookies acceptés !')
}, function () {
  console.log('cookies refusés')
})
cookieBanner.init()
console.log(cookieBanner.isAcceptCookies)
