const defaultParams = {
  id: 'G-XXXXXX',
  enableGtag: true,
  alwaysShowBanner: false,
  generateHTML: true,
  idAccept: 'cookieAccept',
  idRefuse: 'cookieRefuse',
  idClose: 'cookieClose',
  title: 'Nous utilisons des cookies',
  text: 'Nous améliorons ainsi votre expérience de navigation sur notre site web. Les cookies nous permettent de vous fournir des fonctionnalités importantes liées à la sécurité, à la gestion du web et/ou à l’accessibilité.'
}

class CookieFZ {
  constructor (params, onCookiesAccepted, onCookiesRefused) {
    this.params = Object.assign(defaultParams, params)
    this.onCookiesAccepted = onCookiesAccepted
    this.onCookiesRefused = onCookiesRefused
  }

  init () {
    const html = `
      <div id="cookieBanner">
        <div id="cookieBannerWrapper">
          <a href="javascript:" id="${this.params.idClose}" class="close"></a>
          <h6>${this.params.title}</h6>
          <p>${this.params.text}</p>
          <div class="cookie-btn-wrapper">
            <div>
              <a href="javascript:" id="${this.params.idAccept}" class="cookie-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path id="CheckCircle" d="M32,20A12,12,0,1,1,20,8,12,12,0,0,1,32,20ZM18.612,26.354l8.9-8.9a.774.774,0,0,0,0-1.095L26.42,15.261a.774.774,0,0,0-1.095,0l-7.261,7.261-3.39-3.39a.774.774,0,0,0-1.095,0l-1.095,1.095a.774.774,0,0,0,0,1.095l5.032,5.032a.774.774,0,0,0,1.095,0Z" transform="translate(-8 -8)"/>
                </svg>
                Accepter
              </a>
              <a href="javascript:" id="${this.params.idRefuse}" class="cookie-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path id="CrossCircle" data-name="Tracé 1" d="M20,8A12,12,0,1,0,32,20,12,12,0,0,0,20,8Zm4.926,8.99L21.916,20l3.01,3.01a.581.581,0,0,1,0,.823l-1.094,1.094a.581.581,0,0,1-.823,0L20,21.916l-3.01,3.01a.581.581,0,0,1-.823,0l-1.094-1.094a.581.581,0,0,1,0-.823L18.084,20l-3.01-3.01a.581.581,0,0,1,0-.823l1.094-1.094a.581.581,0,0,1,.823,0L20,18.084l3.01-3.01a.581.581,0,0,1,.823,0l1.094,1.094a.581.581,0,0,1,0,.823Z" transform="translate(-8 -8)"/>
                </svg>
                Refuser
              </a>
            </div>
            <!--<div>
              <a href="javascript:" id="cookieCustomize" class="cookie-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="23.997" height="24" viewBox="0 0 23.997 24">
                  <g id="Groupe_8610" data-name="Groupe 8610" transform="translate(-412 -762.267)">
                    <path id="Biscuit" data-name="Tracé 2" d="M23.919,11.987A6,6,0,0,1,17.992,6,6,6,0,0,1,12,.069a6.186,6.186,0,0,0-3.736.6L5.023,2.32A6.2,6.2,0,0,0,2.315,5.029L.67,8.257a6.216,6.216,0,0,0-.6,3.8l.566,3.575a6.213,6.213,0,0,0,1.742,3.421l2.567,2.566a6.191,6.191,0,0,0,3.408,1.737l3.6.569a6.183,6.183,0,0,0,3.774-.6l3.24-1.65a6.2,6.2,0,0,0,2.708-2.709l1.645-3.228a6.214,6.214,0,0,0,.605-3.749ZM8.241,17.245a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,8.241,17.245Zm1.5-7.5a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,9.741,9.746Zm7.5,6a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,17.239,15.745Z" transform="translate(412.008 762.269)"/>
                    <path id="Chocolat" data-name="Tracé 3" d="M154.5,150a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,154.5,150Zm-7.5-6a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,147,144Zm-1.5,7.5A1.5,1.5,0,1,0,147,153,1.5,1.5,0,0,0,145.5,151.5Z" transform="translate(274.749 625.016)"/>
                  </g>
                </svg>
                Personnaliser
              </a>
            </div>-->
          </div>
        </div>
      </div>
    `

    if (this.params.generateHTML && (this.params.alwaysShowBanner || !localStorage.getItem('acceptCookies'))) {
      document.body.insertAdjacentHTML('beforeend', html)
    }

    const cookieRefuse = document.getElementById(this.params.idRefuse)
    const cookieAccept = document.getElementById(this.params.idAccept)
    // const cookieCustomize = document.getElementById('cookieCustomize')
    const cookieClose = document.getElementById(this.params.idClose)

    cookieAccept && cookieAccept.addEventListener('click', this.acceptCookies.bind(this))
    cookieRefuse && cookieRefuse.addEventListener('click', this.refuseCookies.bind(this))
    cookieClose && cookieClose.addEventListener('click', this.closeBanner.bind(this))
  }

  initGtag () {
    window.dataLayer = window.dataLayer || []
    function gtag () {
      window.dataLayer.push(arguments)
    }
    gtag('js', new Date())
    gtag('config', this.params.id)
  }

  acceptCookies () {
    localStorage.setItem('acceptCookies', 'true')
    this.closeBanner()
    if (this.params.enableGtag) {
      this.initGtag()
    }
    this.onCookiesAccepted()
  }

  refuseCookies () {
    localStorage.setItem('acceptCookies', 'false')
    this.closeBanner()
    this.onCookiesRefused()
  }

  closeBanner () {
    if (this.params.alwaysShowBanner) return

    const cookieBanner = document.getElementById('cookieBanner')
    cookieBanner && cookieBanner.remove()
  }

  get isAcceptCookies () {
    return localStorage.getItem('acceptCookies') === 'true'
  }
}

export const create = function (params, onCookiesAccepted, onCookiesRefused) {
  return new CookieFZ(params, onCookiesAccepted, onCookiesRefused)
}

export default CookieFZ
