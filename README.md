# Compile
```
yarn build
```

# Installation
Import CookieFZ class :
```js
import CookieFZ from 'path/to/cookie-fz/dist/cookiefz'
```

Import default style :
```scss
@import "path/to/cookie-fz/dist/cookie-fz.css";
```

Import SASS file to change variables :
```scss
@import "path/to/cookie-fz/src/scss/cookie-fz";
```

# Usage in a script file
Create class instance :
```js
const cookieBanner = new CookieFZ(params, onCookieAccept, onCookieRefuse);
```

Then, generate HTML in page if necessary
```js
cookieBanner.init()
```

Example :
```js
const params = {
    id: 'G-XXXXXX',
    alwaysShowBanner: true
}

const cookieBanner = new CookieFZ(params, function () {
  console.log('cookies accepted !')
}, function () {
  console.log('cookies refused')
})
cookieBanner.init()
```

Usage example in HTML file :
``` html
<script>
    const params = {
        alwaysShowBanner: true
    }

    const cookieBanner = CookieFZ.create(params, function() {
        console.log("cookies acceptés")
    }, function() {
        console.log("cookies refusés")
    })
    cookieBanner.init()
</script>
```

# Parameters
| Name             | Type   | Description                                                                             | Défault value                                                                                                                                                                                                          |
|------------------|--------|-----------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id               | string | product ID                                                                              | G-XXXXXX                                                                                                                                                                                                               |
| enableGtag       | bool   | Utiliser le Global site tag                                                             | true                                                                                                                                                                                                                   |
| alwaysShowBanner | bool   | Toujours afficher la bannière                                                           | false                                                                                                                                                                                                                  |
| generateHTML     | bool   | Utiliser le code HTML par défaut                                                        | true                                                                                                                                                                                                                   |
| idAccept         | string | Si on utilise notre propre code HTML, définir l'id de l'élément pour accepter           | cookieAccept                                                                                                                                                                                                           |
| idRefuse         | string | Si on utilise notre propre code HTML, définir l'id de l'élément pour refuser            | cookieRefuse                                                                                                                                                                                                           |
| idClose          | string | Si on utilise notre propre code HTML, définir l'id de l'élément pour fermer la bannière | cookieClose                                                                                                                                                                                                            |
| title            | string | Titre de la bannière                                                                    | Nous utilisons des cookies                                                                                                                                                                                             |
| text             | string | Contenu texte de la bannière                                                            | Nous améliorons ainsi votre expérience de navigation sur notre site web. Les cookies nous permettent de vous fournir des fonctionnalités importantes liées à la sécurité, à la gestion du web et/ou à l’accessibilité. |
